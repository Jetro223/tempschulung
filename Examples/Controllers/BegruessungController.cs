﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Examples.Controllers
{
    public class UhrzeitMeldung
    {
        public string Nachricht { get; set; }
        public string Uhrzeit { get; set; }
    }

    [Produces("application/json")]
   
    public class BegruessungController : Controller
    {
        [HttpGet]
        [Route("api/Begruessung/{language}")]
        public IActionResult GetBegruessung(string language, string restparameter)
        {
            var aktuelleUhrzeit = DateTime.Now;
            string meldung = "";
            if (aktuelleUhrzeit.Hour >= 0 && aktuelleUhrzeit.Hour < 12)
                meldung = "Guten Morgen";

            if (aktuelleUhrzeit.Hour >= 12 && aktuelleUhrzeit.Hour < 15)
                meldung = "Mahlzeit!";

            if (aktuelleUhrzeit.Hour >= 15)
                meldung = "Guten Abend";

            //var uhrzeitMeldung = new UhrzeitMeldung
            //{
            //    Nachricht = meldung + " " + language + " " + restparameter,
            //    Uhrzeit = aktuelleUhrzeit.ToString("HH:mm")
            //};

            return Ok(new {Nachricht = meldung, Uhrzeit = aktuelleUhrzeit.ToString("HH:mm")});
        }

    }
}