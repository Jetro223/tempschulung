﻿using Microsoft.AspNetCore.Mvc;

namespace Examples.Controllers
{
    public class Person
    {
        public string Name { get; set; }
        public int Alter { get; set; }
        public decimal Schuhgroesse { get; set; }

        public int AlterMalZwei
        {
            get { return Alter * 2; }
        }

        public int GetAlterMalZwei()
        {
            return Alter * 2;
        }
    }

  //  [Produces("application/xml")]
    [Route("api/MyJson")]
    public class HelloWorldJsonController : Controller
    {
        [HttpGet]
        public IActionResult GetJson()
        {
            var person = new Person
            {
                Name = "Max Mustermann",
                Alter = 35,
                Schuhgroesse = 44.5M
            };

            return Ok(person);
        }

    }
}