﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Examples.Controllers
{
    [Route("api/[controller]")]
    public class HelloWorldController : Controller
    {
        [HttpGet]
        public IActionResult HalloWelt()
        {
            // Verhalten
            //...
            //...
            return Content("Hallo Welt!");
        } 

        
        public IActionResult TestZwei()
        {
            // Verhalten
            //...
            //...
            return Content("Bye Welt!");
        }
    }
}
