﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Examples.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Examples.Controllers
{
    public class Ops
    {
        public string OPS_Code { get; set; }
        public string Geschlecht { get; set; }
        public int Jahr { get; set; }
        public string Altersgruppe { get; set; }
        public string Wert { get; set; }
    }

    [Produces("application/json")]
    [Route("api/SqlData")]
    public class SqlDataController : Controller
    {
        private readonly IOptions<ConnectionConfig> _config;
        private readonly DataAccessService _dataAccessService;

        public SqlDataController(IOptions<ConnectionConfig> config, DataAccessService dataAccessService)
        {
            _config = config;
            _dataAccessService = dataAccessService;
        }

        public IActionResult GetData(string altersgruppe)
        {
            var returnValue = _dataAccessService.GetData();
            return Ok(returnValue);
        }

        [Route("procedure")]
        public IActionResult GetDataFromProcedure(string altersgruppe)
        {
            var returnValue = new List<Ops>();
            using (IDbConnection db = new SqlConnection(_config.Value.Dapper))
            {
                returnValue = db.Query<Ops>("sp_GetData", new { myparam = altersgruppe }, commandType: CommandType.StoredProcedure).ToList();
            }

            return Ok(returnValue);
        }

        [Route("dynamic")]
        public IActionResult GetDataFromProcedureDynamic(string tablename)
        {
            var returnValue = new List<dynamic>();
            using (IDbConnection db = new SqlConnection(_config.Value.Dapper))
            {
                returnValue = db.Query<dynamic>("SELECT TOP 10 * FROM " + tablename, new { myparam = "unter 1" }, commandType: CommandType.Text).ToList();
            }

            return Ok(returnValue);
        }
    }
}