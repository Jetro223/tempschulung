﻿using Microsoft.AspNetCore.Mvc;

namespace Examples.Controllers
{
    public class RoutedController : Controller
    {
        public IActionResult Get()
        {
            return Content("Hallo, ich bin der RoutedController");
        }
    }

    public class BetterController : Controller
    {
        public IActionResult Get()
        {
            return Content("Hallo, ich bin besser");
        }
    }

    public class Werbemittel
    {
        public string WbmKategorie { get; set; }
        public string Wbm { get; set; }
    }

    [Route("api/mydata")]
    public class MyController : Controller
    {
        [Route("werbemittel")]
        [HttpGet]
        public IActionResult Get()
        {
            return Content("Ich bin ein Werbemittel");
        }

        [Route("werbemittel/{wbmkategorie}/{wbm}")]
        [HttpGet]
        public IActionResult GetWerbemittel(string wbmkategorie, string wbm)
        {
            return Content($"Ich bin ein Werbemittel der Kategorie {wbmkategorie} und heiße {wbm}");
        }
    }
}