﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Examples.Controllers
{
    public class Schulung
    {
        public string Name { get; set; }
        public Address Adresse { get; set; }
        public List<Teilnehmer> Teilnehmer { get; set; }

        public int AnzahlTeilnehmer
        {
            get { return Teilnehmer.Count; }
        }

        public Schulung()
        {
            Adresse = new Address("Default", "Default");
            Teilnehmer = new List<Teilnehmer>();
        }
    }

    public class Address
    {
        public string Ort { get; set; }
        public string Plz { get; set; }

        public Address(string ort, string plz)
        {
            Ort = ort;
            Plz = plz;
        }
    }

    public class Teilnehmer
    {
        public string Name { get; set; }
        public int Anzahl { get; set; }
    }

    [Produces("application/json")]
    [Route("api/HelloWorldComplex")]
    public class HelloWorldComplexController : Controller
    {
        public IActionResult GetSchulung(Teilnehmer teilnehmer)
        {
            var schulung = new Schulung
            {
                Name = "ASP .NET Core",
                Adresse = new Address("Zirndorf", "90513"),
                Teilnehmer = new List<Teilnehmer>
                {
                    new Teilnehmer
                    {
                        Name = "Maria",
                        Anzahl = 12
                    },
                    new Teilnehmer
                    {
                        Name = "Max",
                        Anzahl = 23
                    },
                    teilnehmer
                }
            };

            return Ok(schulung);
        }
    }
}