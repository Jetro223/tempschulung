﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Examples.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Examples.Services
{
    public class DataAccessService
    {
        private readonly IOptions<ConnectionConfig> _config;

        public DataAccessService(IOptions<ConnectionConfig> config)
        {
            _config = config;
        }

        public List<dynamic> GetDataFromTable(string table)
        {
            var returnValue = new List<dynamic>();
            using (IDbConnection db = new SqlConnection(_config.Value.Dapper))
            {
                returnValue = db.Query<dynamic>("Select TOP 10 * From Ops2016").ToList();
            }

            return returnValue;
        }
    }
}
